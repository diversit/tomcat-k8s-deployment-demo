package k8s.tomcat.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.Inet4Address;
import java.net.UnknownHostException;

@Controller
public class MainController {

    @GetMapping({"/", "/index"})
    public String index(Model model) throws UnknownHostException {
        model.addAttribute("runtime", Runtime.getRuntime());
        model.addAttribute("hostname", Inet4Address.getLocalHost().getHostName());
        return "index";
    }
}
