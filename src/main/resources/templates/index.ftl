<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
</head>
<body>
    <h2 class="hello-title">Hello in ${hostname}</h2>
    <p>
        Available processors: ${runtime.availableProcessors()}<br/>
        Free memory: ${runtime.freeMemory()}<br/>
        Maximum memory: ${runtime.maxMemory()}<br/>
        Total memory: ${runtime.totalMemory()}<br/>
    </p>
</body>
</html>