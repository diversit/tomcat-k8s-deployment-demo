FROM alpine:latest

RUN mkdir /app

ARG WAR_FILE

COPY target/${WAR_FILE} /app/ROOT.war
