# README

This is a demo project to show how a project producing a WAR file
can be using in a Kubernetes setting.


## The setup

Instead of creating a single image with a application platform to run the
WAR file, e.g. Tomcat, the WAR application is packaged in a separate Docker image.

For the application platform, e.g. Tomcat, the default available Docker image can be used.

In the K8S Pod:
- a shared volume, based on `emptyDir`, is used to share data between containers.
- the init-container copies the WAR file to the shared volume.
- the application platform container uses the shared volume as its webapps and so deploys the application.

Note: the application platform config could also be deployed and versioned in the same way.
You just have to figure out how tell the application platform where to find its config. 

## Advantages

By separating the WAR content from the Tomcat it is:
- easy to update Tomcat independant of the application
- simple to update the application
- the container with the custom code is small and gets deployed quickly
- can use the standard K8S concepts service and deployment to easily scale the application

# Demo

_This assumes a local Kubernetes environment is running. E.g. via Docker-for-Mac or Windows or Minikube.
And Helm is installed and ready to be used._

Build the application: `mvn package`  
_This produces an image `diversit/tomcat-war-demo` with the tags `latest`, `<project.version>` and `<git-commit>`._

Deploy the Helm Chart: `helm install chart/ --name=demo`  
_A deployment with a single container is deployed including a NodePort service so the service can easily be displayed.
Check `kubectl get svc` for the public service port._

Make a change in the `index.ftl` file and create a Git-commit with the change.
Re-build the project to create a new Docker image: `mvn package`
Check `docker images | head -3` for the git commit hash on the new Docker image.

Update the Helm deployment with the new image:
`helm upgrade demo chart/ --set war.image.tag=<docker-image-git-commit-tag>`  
_The K8S deployment rolls out the new version by creating 
a new ReplicaSet. First new containers are added before the old containers are terminated._

**Alternative:** increase the number of replica's before rolling out the new image
and see the application containers change 1 by 1.
`kubectl scale deploy/demo --replicas=<nr>`